FILE=paper
OBJDIR=obj
BIBFILE=$(FILE).bib
INFILE=$(FILE).tex
TEXFILE=$(OBJDIR)/$(FILE).tex
OUTFILE=$(FILE).pdf
LATEXFLAGS= -output-directory $(OBJDIR)

$(OUTFILE): $(TEXFILE) $(BIBFILE)
	@pdflatex $(LATEXFLAGS) -draftmode $(TEXFILE)
	@bibtex $(OBJDIR)/$(FILE)
	@pdflatex $(LATEXFLAGS) -draftmode $(TEXFILE)
	@pdflatex $(LATEXFLAGS) $(TEXFILE)
	@mv $(OBJDIR)/$(OUTFILE) ./$(OUTFILE)

$(TEXFILE): $(INFILE)
	@mkdir -p $(OBJDIR)
	lhs2TeX --agda $(INFILE) > $(TEXFILE)

clean:
	$(RM) $(OBJDIR)/*
