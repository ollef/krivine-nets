module Backend(compile) where

import Control.Applicative
import Control.Monad.Trans.RWS
import Data.List
import Data.Monoid
import Data.Text(Text)
import qualified Data.Text as T

import AST
import Auxiliary
import ByteCode hiding (compile)

type CName = Int

cName :: CName -> Text
cName = s . ('f' :) . show

data CompiledFun = CompiledFun
  { cfName    :: CName
  , cfCode    :: CodeF CName
  } deriving (Show, Eq)

type Linker a = RWS () [CompiledFun] [CName] a

runLinker :: Linker a -> (a, [CompiledFun])
runLinker l = evalRWS l () [0..]

newCompiledFun :: Code -> Linker CName
newCompiledFun c = do
  lc <- link c
  n:ns <- get
  put ns
  tell [CompiledFun
    { cfName = n
    , cfCode = lc
    }]
  return n

link :: Code -> Linker (CodeF CName)
link (In code) = case code of
  i :. c        -> (:.) <$> linkInstr i <*> link (In c)
  CLit lit      -> return $ CLit lit
  Access v      -> return $ Access v
  Prim p c1 c2  -> Prim p <$> newCompiledFun c1 <*> newCompiledFun c2
  Cond c1 c2 c3 -> Cond <$> newCompiledFun c1 <*> newCompiledFun c2 <*> newCompiledFun c3
  where
    linkInstr :: InstrF Code -> Linker (InstrF CName)
    linkInstr instr = case instr of
      PopArg    -> return PopArg
      PushArg c -> PushArg <$> newCompiledFun c

s :: String -> Text
s = T.pack
sshow :: Show a => a -> Text
sshow = s . show

indent :: [Text] -> [Text]
indent = map (s"\t" <>)

scope :: Text -> [Text] -> [Text]
scope f b | T.null f = mconcat
  [ [s"{"]
  , indent b
  , [s"}"]
  ]
scope f b | otherwise = mconcat
  [ [f `space` s"{"]
  , indent b
  , [s"}"]
  ]

parens :: [Text] -> Text
parens xs = s"("
  <> mconcat (intersperse (s", ") xs)
  <> s")"

space :: Text -> Text -> Text
space a b = a <> s" " <> b

decl :: String -> Text -> Text
decl t x   = s t `space` x

dStack, dEnv, dVoid :: Text -> Text
dStack = decl "Stack"
dEnv   = decl "Env"
dVoid  = decl "void"

env, stack :: Text
env    = s"env"
stack  = s"stack"

compileCode :: CodeF CName -> [Text]
compileCode code = case code of
  i :. c          -> compileInstr i : compileCode c
  CLit (LitInt n) -> [lit [stack, sshow n]]
  Access v        -> [access [env, stack, sshow v]]
  Prim op c1 c2   -> [primop [env, stack, prim op, cName c1, cName c2]]
  Cond c1 c2 c3   -> [cond [env, stack, cName c1, cName c2, cName c3]]
  where
    compileInstr :: InstrF CName -> Text
    compileInstr instr = case instr of
      PopArg    -> popArg  [env, stack]
      PushArg c -> pushArg [cName c, env, stack]

    call  a bs = s a <> parens bs
    scall a bs = call a bs <> s";"
    popArg  = scall "instr_pop_arg"
    pushArg = scall "instr_push_arg"
    lit     = scall "instr_lit"
    access  = scall "instr_access"
    primop  = scall "instr_primop"
    cond    = scall "instr_cond"
    prim Add = s"op_add"
    prim Mul = s"op_mul"
    prim Sub = s"op_sub"
    prim Div = s"op_div"
    prim Eq  = s"op_eq"
    prim NEq = s"op_neq"
    prim Gr  = s"op_gr"
    prim Le  = s"op_le"
    prim GEq = s"op_geq"
    prim LEq = s"op_leq"

compile :: Int -> Code -> Text
compile es code = mkNode
  where
    (startCode, cfs) = runLinker (newCompiledFun code)
    mkNode = T.unlines
             $ [header]
             ++ [mkFunDecl (cfName cf) <> s";" | cf <- cfs]
             ++ mconcat [mkFun (cfName cf) (compileCode $ cfCode cf) | cf <- cfs]
             ++ mkMain
    mkFunDecl name = dVoid (cName name) <> parens [dEnv env, dStack stack]
    mkFun name c = mkFunDecl name `scope` (s"PRINT_DEBUG_INFO;" : c)
    header = s"#include \"prelude.h\""
    mkMain = s"int main(int argc, char** argv)" `scope`
      [ s"Env env = malloc_env(" <> sshow es <>  s");"
      , s"Stack stack = malloc_stack();"
      , cName startCode <> s"(env, stack);"
      , s"return 0;"
      ]
