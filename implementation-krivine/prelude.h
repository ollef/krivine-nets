#ifndef __PRELUDE_H__
#define __PRELUDE_H__

#define __STDC_FORMAT_MACROS
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define MALLOC malloc
#define ERROR(...) fprintf(stderr, "********" __VA_ARGS__); exit(0);
#ifdef NDEBUG
#define PRINT_DEBUG(...) printf(__VA_ARGS__);
#define PRINT_DEBUG_INFO {PRINT_DEBUG("entering %s\n", __FUNCTION__);}
#else
#define PRINT_DEBUG(...)
#define PRINT_DEBUG_INFO
#endif

/* An int of the architecture's pointer size */
typedef intptr_t int_t;

struct Env_t; typedef struct Env_t* Env;
struct Stack_t; typedef struct Stack_t* Stack;

static const size_t STACK_SIZE = 10000;

typedef void(*Code)(Env, Stack);
/* ----------------------------------------------------------------------------
 * Closures
 * ---------------------------------------------------------------------------*/
typedef struct Closure {
  Code code;
  Env env;
} Closure;

/* ----------------------------------------------------------------------------
 * Environments
 * ---------------------------------------------------------------------------*/
struct Env_t {
  size_t max_size;
  Closure* top;
  Closure* bottom;
};

static inline size_t size_env(Env env) {
  PRINT_DEBUG_INFO;
  return env->bottom - env->top;
}

static inline void print_env(Env env) {
#ifdef NDEBUG
  PRINT_DEBUG("Env: %zu\n", size_env(env));
#endif
}

static inline Closure* access_env(Env env, size_t index) {
  PRINT_DEBUG_INFO;
  return env->top + index;
}

static inline void push_env(Env env, Closure val) {
  PRINT_DEBUG_INFO;
  *env->bottom = val;
  ++env->bottom;
}

static inline Env malloc_env(size_t max_size) {
  PRINT_DEBUG_INFO;
  Closure* arr = MALLOC(sizeof(Closure) * max_size);
  Env res = MALLOC(sizeof(struct Env_t));
  res->top = res->bottom = arr;
  res->max_size = max_size;
  return res;
}

static inline Env copy_env(Env env) {
  PRINT_DEBUG_INFO;
  Env copy = malloc_env(env->max_size);
  for(Closure* top = env->top; top < env->bottom; ++top) {
    push_env(copy, *top);
  }
  print_env(env);
  return copy;
}


void enter_closure(Closure closure, Stack stack) {
  closure.code(closure.env, stack);
}

void enter_closure_copy(Closure closure, Stack stack) {
  closure.code(copy_env(closure.env), stack);
}

/* ----------------------------------------------------------------------------
 * Stack elements
 * ---------------------------------------------------------------------------*/
typedef enum StackElemTag {
  ARG,
  IF,
  OP2,
  OP1
} StackElemTag;

typedef struct IfStackElem {
  Closure truecase;
  Closure falsecase;
} IfStackElem;

typedef struct Op2StackElem {
  int_t (*op)(int_t, int_t);
  Closure arg;
} Op2StackElem;

typedef struct Op1StackElem {
  int_t (*op)(int_t, int_t);
  int_t arg;
} Op1StackElem;

typedef struct {
  StackElemTag tag;
  union {
    Closure arg;
    IfStackElem iff;
    Op2StackElem op2;
    Op1StackElem op1;
  } payload;
} StackElem;

StackElem arg_stackelem(Closure arg) {
  return (StackElem) {.tag = ARG, .payload = {.arg = arg}};
}

void print_stackelem(StackElem el) {
#ifdef NDEBUG
  switch(el.tag) {
    case ARG:
      PRINT_DEBUG("arg");
      break;
    case IF:
      PRINT_DEBUG("if");
      break;
    case OP2:
      PRINT_DEBUG("op2");
      break;
    case OP1:
      PRINT_DEBUG("op1");
      break;
  }
#endif
}

/* ----------------------------------------------------------------------------
 * Stacks
 * ---------------------------------------------------------------------------*/
struct Stack_t {
  StackElem* top;
  StackElem* bottom; // One step further than last element
};

static inline void print_stack(Stack stack) {
#ifdef NDEBUG
  PRINT_DEBUG("Stack: ");
  for(StackElem* v = stack->bottom - 1; v >= stack->top; --v) {
    print_stackelem(*v); PRINT_DEBUG(", ");
  }
  PRINT_DEBUG("[]\n");
#endif
}

static inline Stack malloc_stack() {
  PRINT_DEBUG_INFO;
  StackElem* arr = MALLOC(sizeof(StackElem) * STACK_SIZE);
  Stack res = MALLOC(sizeof(struct Stack_t));
  res->top = res->bottom = arr;
  return res;
}

static inline StackElem* pop_stack(Stack stack) {
  PRINT_DEBUG_INFO;
  if(stack->bottom <= stack->top) {
    return 0;
  }
  --stack->bottom;
  print_stack(stack);
  return stack->bottom;
}

static inline Closure pop_arg(Stack stack) {
  PRINT_DEBUG_INFO;
  StackElem* el = pop_stack(stack);
  if(el->tag != ARG) {
    ERROR("Trying to pop a stack without args");
  }
  return el->payload.arg;
}

static inline void push_stack(Stack stack, StackElem val) {
  PRINT_DEBUG_INFO;
  *stack->bottom = val;
  ++stack->bottom;
  print_stack(stack);
}

/* ----------------------------------------------------------------------------
 * Instructions
 * ---------------------------------------------------------------------------*/
static inline void instr_pop_arg(Env env, Stack stack) {
  PRINT_DEBUG_INFO;
  push_env(env, pop_arg(stack));
}

static inline void instr_push_arg(Code code, Env env, Stack stack) {
  PRINT_DEBUG_INFO;
  StackElem sel = arg_stackelem((Closure) {.code = code, .env = copy_env(env)});
  push_stack(stack, sel);
}

static inline void instr_lit(Stack stack, int_t n) {
  PRINT_DEBUG_INFO;
  PRINT_DEBUG("lit: %" PRIdPTR "\n", n);
  StackElem* elptr = pop_stack(stack);
  if(elptr == 0) {
    printf("RESULT: %" PRIdPTR "\n", n);
    return;
  }
  StackElem el = *elptr;
  switch(el.tag) {
    case IF: {
      IfStackElem iff = el.payload.iff;
      if(n) {
        enter_closure(iff.truecase,  stack);
      } else {
        enter_closure(iff.falsecase, stack);
      }
      break;
    }
    case OP2: {
      Op2StackElem op2 = el.payload.op2;
      Op1StackElem op1 = {.op = op2.op, .arg = n};
      push_stack(stack, (StackElem) {.tag = OP1, .payload = {.op1 = op1}});
      enter_closure(op2.arg, stack);
      break;
    }
    case OP1: {
      Op1StackElem op1 = el.payload.op1;
      instr_lit(stack, op1.op(op1.arg, n));
      break;
    }
    default:
      ERROR("LITERAL WITHOUT CONTINUATION");
  }
}

static inline void instr_access(Env env, Stack stack, int_t index) {
  PRINT_DEBUG_INFO;
  PRINT_DEBUG("env index: %" PRIdPTR "\n", index);
  Closure* c = access_env(env, index);
  enter_closure_copy(*c, stack);
}

static inline void instr_primop(Env env, Stack stack, int_t (*op)(int_t, int_t), Code c1, Code c2) {
  PRINT_DEBUG_INFO;
  push_stack(stack, (StackElem) {.tag = OP2, 
                                 .payload = {.op2 = {.op = op,
                                                     .arg = {.code = c2,
                                                             .env = copy_env(env)}}}});
  c1(env, stack);
}

static inline void instr_cond(Env env, Stack stack, Code c1, Code c2, Code c3) {
  PRINT_DEBUG_INFO;
  Env envcopy = copy_env(env);
  push_stack(stack, (StackElem) {.tag = IF,
                                 .payload = {.iff = {.truecase  = {.code = c2,
                                                                   .env = envcopy},
                                                     .falsecase = {.code = c3,
                                                                   .env = envcopy}}}});
  c1(env, stack);
}

/* ----------------------------------------------------------------------------
 * Operations
 * ---------------------------------------------------------------------------*/
static inline int_t op_add(int_t x, int_t y) {
  return x + y;
}

static inline int_t op_mul(int_t x, int_t y) {
  return x * y;
}

static inline int_t op_sub(int_t x, int_t y) {
  return x - y;
}

static inline int_t op_div(int_t x, int_t y) {
  return x / y;
}

static inline int_t op_eq(int_t x, int_t y) {
  return x == y;
}

static inline int_t op_neq(int_t x, int_t y) {
  return x != y;
}

static inline int_t op_gr(int_t x, int_t y) {
  return x > y;
}

static inline int_t op_le(int_t x, int_t y) {
  return x < y;
}

static inline int_t op_geq(int_t x, int_t y) {
  return x >= y;
}

static inline int_t op_leq(int_t x, int_t y) {
  return x <= y;
}

#endif
