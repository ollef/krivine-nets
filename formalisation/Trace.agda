module Trace where

open import GeneralLemmas
open import Relation

infixr 4 _⟶⟨_⟩_

data Trace {A}(R : Rel A A) : Rel A A where
  _end : (a : A) → Trace R a a
  _⟶⟨_⟩_ : (a : A){b c : A} → R a b → Trace R b c → Trace R a c

*-to-Trace : ∀ {A}{a b : A}{R} → (R *) a b → Trace R a b
*-to-Trace [] = _ end
*-to-Trace (x ∷ steps) = _ ⟶⟨ x ⟩ *-to-Trace steps

det-Trace : ∀ {A}{R : Rel A A}(steps : ℕ)(a : A) → R is-decidable → ∃ λ b → Trace R a b
det-Trace zero a dec = a , a end
det-Trace (suc steps) a dec with dec a
det-Trace (suc steps) a dec | yes (b , step) =
  let (c , steps) = det-Trace steps b dec
   in c , a ⟶⟨ step ⟩ steps
det-Trace (suc steps) a dec | no _ = a , a end

data Node : Set where
  A : Node
  B : Node

open import Krivine Node
open import Krivine.Properties Node
open import Lambda Node
open import Data.Nat

termExample : Term
termExample = ƛ (ƛ (v₀ +' v₁)) $ lit 3 $ lit 4
  where
    v₀ = var 0
    v₁ = var 1
    _+'_ = op _+_

termExample-trace : ∃ (λ b → Trace _⟶Krivine_ ((termExample , [] , [])) b)
termExample-trace = det-Trace 100 (termExample , [] , []) decidable-Krivine

termExample-result = proj₁ termExample-trace

{-
((ƛ (ƛ op _+_ (var 0) (var 1)) $ lit 3 $ lit 4) , [] , [])
⟶⟨ PUSHARG ⟩
((ƛ (ƛ op _+_ (var 0) (var 1)) $ lit 3) , [] , arg (lit 4 , []) ∷ [])
⟶⟨ PUSHARG ⟩
(ƛ (ƛ op _+_ (var 0) (var 1)) , [] , arg (lit 3 , []) ∷ arg (lit 4 , []) ∷ [])
⟶⟨ POPARG ⟩
(ƛ op _+_ (var 0) (var 1) , clos (lit 3 , []) ∷ [] , arg (lit 4 , []) ∷ [])
⟶⟨ POPARG ⟩
(op _+_ (var 0) (var 1) , clos (lit 4 , []) ∷ clos (lit 3 , []) ∷ [] , [])
⟶⟨ OP ⟩
(var 0 , clos (lit 4 , []) ∷ clos (lit 3 , []) ∷ [] , op₂ _+_ (var 1 , clos (lit 4 , []) ∷ clos (lit 3 , []) ∷ []) ∷ [])
⟶⟨ VAR refl ⟩
(lit 4 , [] , op₂ _+_ (var 1 , clos (lit 4 , []) ∷ clos (lit 3 , []) ∷ []) ∷ [])
⟶⟨ OP₂ ⟩
(var 1 , clos (lit 4 , []) ∷ clos (lit 3 , []) ∷ [] , op₁ (_+_ 4) ∷ [])
⟶⟨ VAR refl ⟩
(lit 3 , [] , op₁ (_+_ 4) ∷ [])
⟶⟨ OP₁ ⟩
(lit 7 , [] , [])
end
-}

-- (λf x. f x)@B (λy. y) 0
termExample' : Term
termExample' = ((ƛ (ƛ (v₁ $ v₀))) at B) $ (ƛ v₀) $ lit 0
  where
    v₀ = var 0
    v₁ = var 1

termExample'-trace : ∃ (λ b → Trace _⟶Krivine_ ((termExample' , [] , [])) b)
termExample'-trace = det-Trace 100 (termExample' , [] , []) decidable-Krivine

{-
(((ƛ (ƛ (var 1 $ var 0))) at B $ ƛ var 0 $ lit 0) , [] , [])
⟶⟨ PUSHARG ⟩
(((ƛ (ƛ (var 1 $ var 0))) at B $ ƛ var 0) , [] , arg (lit 0 , []) ∷ [])
⟶⟨ PUSHARG ⟩
((ƛ (ƛ (var 1 $ var 0))) at B , [] , arg (ƛ var 0 , []) ∷ arg (lit 0 , []) ∷ [])
⟶⟨ REMOTE ⟩
(ƛ (ƛ (var 1 $ var 0)) , [] , arg (ƛ var 0 , []) ∷ arg (lit 0 , []) ∷ [])
⟶⟨ POPARG ⟩
(ƛ (var 1 $ var 0) , clos (ƛ var 0 , []) ∷ [] , arg (lit 0 , []) ∷ [])
⟶⟨ POPARG ⟩
((var 1 $ var 0) , clos (lit 0 , []) ∷ clos (ƛ var 0 , []) ∷ [] , [])
⟶⟨ PUSHARG ⟩
(var 1 , clos (lit 0 , []) ∷ clos (ƛ var 0 , []) ∷ [] , arg (var 0 , clos (lit 0 , []) ∷ clos (ƛ var 0 , []) ∷ []) ∷ [])
⟶⟨ VAR refl ⟩
(ƛ var 0 , [] , arg (var 0 , clos (lit 0 , []) ∷ clos (ƛ var 0 , []) ∷ []) ∷ [])
⟶⟨ POPARG ⟩
(var 0 , clos (var 0 , clos (lit 0 , []) ∷ clos (ƛ var 0 , []) ∷ []) ∷ [] , [])
⟶⟨ VAR refl ⟩
(var 0 , clos (lit 0 , []) ∷ clos (ƛ var 0 , []) ∷ [] , [])
⟶⟨ VAR refl ⟩
(lit 0 , [] , [])
end
-}

{- Distributed:

Node A
just (((ƛ (ƛ (var 1 $ var 0))) at B $ ƛ var 0 $ lit 0) , [] , [] , nothing) , ∅
⟶< PUSHARG >
just (((ƛ (ƛ (var 1 $ var 0))) at B $ ƛ var 0) , [] , arg (lit 0 , []) ∷ [] , nothing) , ∅
⟶< PUSHARG >
just (((ƛ (ƛ (var 1 $ var 0))) at B) , [] , arg (ƛ var 0 , []) ∷ arg (lit 0 , []) ∷ [] , nothing) , ∅
⟶< REMOTE-send >
nothing , {ptr₁ ↦ (arg (ƛ var 0 , []) ∷ arg (lit 0 , []) ∷ [] , nothing)}

sends message: REMOTE (ƛ (ƛ (var 1 $ var 0))) B (ptr₁ , A) 2
(2 is number of args on stack that (ptr₁ , A) points to)

Node B
nothing , ∅
⟶< REMOTE-receive > -- (receives the above message)
just (ƛ (ƛ (var 1 $ var 0)) , [] , [] , just ((ptr₁ , A) , 2 , 0)) , ∅
⟶< POPARG-remote >
just (ƛ (var 1 $ var 0) , remote (ptr₁ , A) 0 ∷ [] , [] , just ((ptr₁ , A) , 1 , 1)) , ∅
⟶< POPARG-remote >
just ((var 1 $ var 0) , remote (ptr₁ , A) 1 ∷ remote (ptr₁ , A) 0 ∷ [] , [] , just ((ptr₁ , A) , 0 , 2)) , ∅
⟶< PUSHARG >
just ( var 1
     , remote (ptr₁ , A) 1 ∷ remote (ptr₁ , A) 0 ∷ []
     , arg (var 0 , remote (ptr₁ , A) 1 ∷ remote (ptr₁ , A) 0 ∷ []) ∷ []
     , just ((ptr₁ , A) , 0 , 2))
   , ∅
⟶< VAR-send >
nothing , {ptr₂ ↦ (arg (var 0 , remote (ptr₁ , A) 1 ∷ remote (ptr₁ , A) 0 ∷ []) ∷ [] , just ((ptr₁ , A) , 0 , 2))}

sends message: VAR (ptr₁ , A) 0 (ptr₂ , B) 1 -- 0 is index into stack pointed to by (ptr₁ , A), 1 is number of args on stack pointed to by (ptr₂ , B)

Node A
  nothing
, {ptr₁ ↦ (arg (ƛ var 0 , []) ∷ arg (lit 0 , []) ∷ [] , nothing)}
⟶< VAR-receive > -- (receives the above message)
just (var 0 , local (ƛ var 0 , []) ∷ [] , [] , just ((ptr₂ , B) , 1 , 0)) -- stack-index 0 of ptr₁ stack is local
, {ptr₁ ↦ (arg (ƛ var 0 , []) ∷ arg (lit 0 , []) ∷ [] , nothing)}
⟶< VAR >
just (ƛ var 0 , [] , [] , just ((ptr₂ , B) , 1 , 0))
, {ptr₁ ↦ (arg (ƛ var 0 , []) ∷ arg (lit 0 , []) ∷ [] , nothing)}
⟶< POPARG-remote >
just (var 0 , remote (ptr₂ , B) 0 ∷ [] , [] , just ((ptr₂ , B) , 0 , 1))
, {ptr₁ ↦ (arg (ƛ var 0 , []) ∷ arg (lit 0 , []) ∷ [] , nothing)}
⟶< VAR-send >
nothing
, {ptr₁ ↦ (arg (ƛ var 0 , []) ∷ arg (lit 0 , []) ∷ [] , nothing), ptr₃ ↦ ([] , just ((ptr₂ , B) , 0 , 1))}

sends message: VAR (ptr₂ , B) 0 (ptr₃ , A) 0 -- 0 is index into stack pointed to by (ptr₂ , B), 0 is number of args on stack pointed to by (ptr₃ , A)

Node B
nothing , {ptr₂ ↦ (arg (var 0 , remote (ptr₁ , A) 1 ∷ remote (ptr₁ , A) 0 ∷ []) ∷ [] , just ((ptr₁ , A) , 0 , 2))}
⟶< VAR-receive > -- (receives the above message)
just (var 0
     , local (var 0 , remote (ptr₁ , A) 1 ∷ remote (ptr₁ , A) 0 ∷ []) ∷ []
     , []
     , just ((ptr₃ , A) , 0 , 0))
, {ptr₂ ↦ (arg (var 0 , remote (ptr₁ , A) 1 ∷ remote (ptr₁ , A) 0 ∷ []) ∷ [] , just ((ptr₁ , A) , 0 , 2))}
⟶< VAR >
just (var 0
     , remote (ptr₁ , A) 1 ∷ remote (ptr₁ , A) 0 ∷ []
     , []
     , just ((ptr₃ , A) , 0 , 0))
, {ptr₂ ↦ (arg (var 0 , remote (ptr₁ , A) 1 ∷ remote (ptr₁ , A) 0 ∷ []) ∷ [] , just ((ptr₁ , A) , 0 , 2))}
⟶< VAR-send > -- var 0 is remote
nothing
, {ptr₂ ↦ (arg (var 0 , remote (ptr₁ , A) 1 ∷ remote (ptr₁ , A) 0 ∷ []) ∷ [] , just ((ptr₁ , A) , 0 , 2))
  ,ptr₄ ↦ ([] , just ((ptr₃ , A) , 0 , 0))}

sends message: VAR (ptr₁ , A) 1 (ptr₄ , B) 0 -- 1 is index into stack pointed to by (ptr₁ , A), 0 is number of args on stack pointed to by (ptr₃ , A)

Node A
nothing
, {ptr₁ ↦ (arg (ƛ var 0 , []) ∷ arg (lit 0 , []) ∷ [] , nothing), ptr₃ ↦ ([] , just ((ptr₂ , B) , 0 , 1))}
⟶< VAR-receive > -- (receives the above message)
just ( var 0
     , local (lit 0 , []) ∷ []
     , []
     , just ((ptr₄ , B) , 0 , 0))
, {ptr₁ ↦ (arg (ƛ var 0 , []) ∷ arg (lit 0 , []) ∷ [] , nothing), ptr₃ ↦ ([] , just ((ptr₂ , B) , 0 , 1))}
⟶< VAR > -- var 0 is local
just (lit 0
     , []
     , []
     , just ((ptr₄ , B) , 0 , 0))
, {ptr₁ ↦ (arg (ƛ var 0 , []) ∷ arg (lit 0 , []) ∷ [] , nothing), ptr₃ ↦ ([] , just ((ptr₂ , B) , 0 , 1))}
⟶< RETURN-send > -- we have a literal and a stack extension without arguments, so we should return it there
nothing
, {ptr₁ ↦ (arg (ƛ var 0 , []) ∷ arg (lit 0 , []) ∷ [] , nothing), ptr₃ ↦ ([] , just ((ptr₂ , B) , 0 , 1))}

sends message:  RETURN (ptr₄ , B) 0 0 -- 0 is the literal we send back, 0 is the number of arguments to drop from stack pointed to by (ptr₄ , B)

Node B
nothing
, {ptr₂ ↦ (arg (var 0 , remote (ptr₁ , A) 1 ∷ remote (ptr₁ , A) 0 ∷ []) ∷ [] , just ((ptr₁ , A) , 0 , 2))
  ,ptr₄ ↦ ([] , just ((ptr₃ , A) , 0 , 0))}
⟶< RETURN-receive > -- receives the above message
just (lit 0
     , []
     , []
     , just ((ptr₃ , A) , 0 , 0))
, {ptr₂ ↦ (arg (var 0 , remote (ptr₁ , A) 1 ∷ remote (ptr₁ , A) 0 ∷ []) ∷ [] , just ((ptr₁ , A) , 0 , 2))
  ,ptr₄ ↦ ([] , just ((ptr₃ , A) , 0 , 0))}
⟶< RETURN-send > -- we have a literal and a stack extension without arguments, so we should return it there
nothing
, {ptr₂ ↦ (arg (var 0 , remote (ptr₁ , A) 1 ∷ remote (ptr₁ , A) 0 ∷ []) ∷ [] , just ((ptr₁ , A) , 0 , 2))
  ,ptr₄ ↦ ([] , just ((ptr₃ , A) , 0 , 0))}

sends message: RETURN (ptr₃ , A) 0 0 -- 0 is the literal we send back, 0 is the number of arguments to drop from stack pointed to by (ptr₃ , A)

Node A
nothing
, {ptr₁ ↦ (arg (ƛ var 0 , []) ∷ arg (lit 0 , []) ∷ [] , nothing), ptr₃ ↦ ([] , just ((ptr₂ , B) , 0 , 1))}
⟶< RETURN-receive > -- (receives the above message)
just (lit 0
     , []
     , []
     , just ((ptr₂ , B) , 0 , 1))
, {ptr₁ ↦ (arg (ƛ var 0 , []) ∷ arg (lit 0 , []) ∷ [] , nothing), ptr₃ ↦ ([] , just ((ptr₂ , B) , 0 , 1))}
⟶< RETURN-send > -- we have a literal and a stack extension without arguments, so we should return it there
nothing
, {ptr₁ ↦ (arg (ƛ var 0 , []) ∷ arg (lit 0 , []) ∷ [] , nothing), ptr₃ ↦ ([] , just ((ptr₂ , B) , 0 , 1))}

sends message: RETURN (ptr₂ , B) 0 1 -- 0 is the literal we send back, 1 is number of arguments to drop from stack pointed to by (ptr₂ , B)

Node B
nothing
, {ptr₂ ↦ (arg (var 0 , remote (ptr₁ , A) 1 ∷ remote (ptr₁ , A) 0 ∷ []) ∷ [] , just ((ptr₁ , A) , 0 , 2))
  ,ptr₄ ↦ ([] , just ((ptr₃ , A) , 0 , 0))}
⟶< RETURN-receive > -- (receives the above message)
just (lit 0
     , []
     , []                         -- we drop the first argument from ptr₂, leaving us with this
     , just ((ptr₁ , A) , 0 , 2))
, {ptr₂ ↦ (arg (var 0 , remote (ptr₁ , A) 1 ∷ remote (ptr₁ , A) 0 ∷ []) ∷ [] , just ((ptr₁ , A) , 0 , 2))
  ,ptr₄ ↦ ([] , just ((ptr₃ , A) , 0 , 0))}
⟶< RETURN-send > -- we have a literal and a stack extension without arguments, so we should return it there
nothing
, {ptr₂ ↦ (arg (var 0 , remote (ptr₁ , A) 1 ∷ remote (ptr₁ , A) 0 ∷ []) ∷ [] , just ((ptr₁ , A) , 0 , 2))
  ,ptr₄ ↦ ([] , just ((ptr₃ , A) , 0 , 0))}

sends message: RETURN (ptr₁ , A) 0 2 -- 0 is the literal we send back, 2 is the number of arguments to drop from stack pointed to by (ptr₁ , A)

Node A, finally
nothing
, {ptr₁ ↦ (arg (ƛ var 0 , []) ∷ arg (lit 0 , []) ∷ [] , nothing), ptr₃ ↦ ([] , just ((ptr₂ , B) , 0 , 1))}
⟶< RETURN-receive > -- (receives the above message)
just (lit 0
     , []
     , []        -- we drop two arguments from ptr₁, leaving us with this
     , nothing)
, {ptr₁ ↦ (arg (ƛ var 0 , []) ∷ arg (lit 0 , []) ∷ [] , nothing), ptr₃ ↦ ([] , just ((ptr₂ , B) , 0 , 1))}

end

Here we can make a point about shortcutting, i.e. it would be more
efficient to not create new heap values for stacks that are just
remote stack extensions -- we might as well just send those stack
extensions directly, which would alleviate the need for some of the
ping-pong message passing above. Note that this is a very simple
optimisation to implement.

Another point to make is about GC (note the garbage left in the heaps)

-}


termExample'-result = proj₁ termExample'-trace

factorial : Term
factorial =
  let
    v₀ = var 0
    v₁ = var 1
    l1 = lit 1
    _*'_ = op _*_
    _-'_ = op _∸_
    Y = let x = ƛ (v₁ $ (v₀ $ v₀))
         in ƛ (x $ x)
    fact = ƛ (ƛ (if0 v₀ then l1 else (v₀ *' (v₁ $ (v₀ -' l1)))))
   in Y $ fact $ lit 5

factorial-trace : ∃ λ b → Trace _⟶Krivine_ (factorial , [] , []) b
factorial-trace = det-Trace 250 (factorial , [] , []) decidable-Krivine

factorial-result = proj₁ factorial-trace
