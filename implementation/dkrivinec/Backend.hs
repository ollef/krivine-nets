module Backend(compile) where

import Control.Applicative
import Control.Monad.Trans.RWS
import Data.List
import Data.Maybe
import Data.Monoid
import Data.Text(Text)
import qualified Data.Text as T

import AST
import Auxiliary
import ByteCode hiding (compile)

type CName = Int

cName :: CName -> Text
cName = s . ('f' :) . show

data CompiledFun = CompiledFun
  { cfName    :: CName
  , cfNode    :: Node
  , cfEnvSize :: Maybe Int
  , cfCode    :: CodeF CName
  } deriving (Show, Eq)

type Linker a = RWS Node [CompiledFun] [CName] a

runLinker :: Linker a -> Node -> (a, [CompiledFun])
runLinker l n = evalRWS l n [0..]

newCompiledFun :: Maybe Int -> Code -> Linker CName
newCompiledFun visible c = do
  lc <- link c
  n:ns <- get
  put ns
  node <- ask
  tell [CompiledFun
    { cfName = n
    , cfNode = node
    , cfEnvSize = visible
    , cfCode = lc
    }]
  return n

link :: Code -> Linker (CodeF CName)
link (In code) = case code of
  i :. c        -> (:.) <$> linkInstr i <*> link (In c)
  CLit lit      -> return $ CLit lit
  Access v      -> return $ Access v
  Prim p c1 c2  -> Prim p <$> ncf c1 <*> ncf c2
  Remote n es c -> Remote n es <$> local (const n) (nct es c)
  Cond c1 c2 c3 -> Cond <$> ncf c1 <*> ncf c2 <*> ncf c3
  where
    ncf = newCompiledFun Nothing
    nct = newCompiledFun . Just
    linkInstr :: InstrF Code -> Linker (InstrF CName)
    linkInstr instr = case instr of
      PopArg    -> return $ PopArg
      PushArg c -> PushArg <$> ncf c

s :: String -> Text
s = T.pack
sshow :: Show a => a -> Text
sshow = s . show

indent :: [Text] -> [Text]
indent = map (s"\t" <>)

scope :: Text -> [Text] -> [Text]
scope f b | T.null f = mconcat
  [ [s"{"]
  , indent b
  , [s"}"]
  ]
scope f b | otherwise = mconcat
  [ [f `space` s"{"]
  , indent b
  , [s"}"]
  ]

parens :: [Text] -> Text
parens xs = s"("
  <> mconcat (intersperse (s", ") xs)
  <> s")"

space :: Text -> Text -> Text
space a b = a <> s" " <> b

decl :: String -> Text -> Text
decl t x   = s t `space` x

dStack, dEnv, dVoid :: Text -> Text
dStack = decl "Stack"
dEnv   = decl "Env"
dVoid  = decl "void"

env, stack :: Text
env    = s"env"
stack  = s"stack"

rankOf :: Node -> Text
rankOf n = s"RANK_" <> s n

compileCode :: CodeF CName -> [Text]
compileCode code = case code of
  i :. c          -> compileInstr i : compileCode c
  CLit (LitInt n) -> [lit [stack, sshow n]]
  Access v        -> [access [env, stack, sshow v]]
  Prim op c1 c2   -> [primop [env, stack, prim op, cName c1, cName c2]]
  Remote n _ c    -> [remote [stack , rankOf n, sshow c]]
  Cond c1 c2 c3   -> [cond [env, stack, cName c1, cName c2, cName c3]]
  where
    compileInstr :: InstrF CName -> Text
    compileInstr instr = case instr of
      PopArg    -> popArg  [env, stack]
      PushArg c -> pushArg [cName c, env, stack]

    call  a bs = s a <> parens bs
    scall a bs = call a bs <> s";"
    popArg  = scall "instr_pop_arg"
    pushArg = scall "instr_push_arg"
    lit     = scall "instr_lit"
    access  = scall "instr_access"
    primop  = scall "instr_primop"
    remote  = scall "instr_remote"
    cond    = scall "instr_cond"
    prim Add = s"op_add"
    prim Mul = s"op_mul"
    prim Sub = s"op_sub"
    prim Div = s"op_div"
    prim Eq  = s"op_eq"
    prim NEq = s"op_neq"
    prim Gr  = s"op_gr"
    prim Le  = s"op_le"
    prim GEq = s"op_geq"
    prim LEq = s"op_leq"

compile :: Node -> Int -> Text -> Code -> [(Node, Text)]
compile startNode es root code = mkRoot : cc
  where
    (startCode, cfs) = runLinker (newCompiledFun (Just es) code) startNode
    cfs' = [(cfNode x, xs) | xs@(x:_) <- sortedGroupBy cfNode cfs]
    ncfs' = zip [(1 :: Int)..] cfs'
    cc = [(node, mkNode funs) | (node, funs) <- cfs']
    mkNode funs = T.unlines
                $ [header]
               ++ [mkFunDecl (cfName cf) <> s";" | cf <- funs]
               ++ mconcat [mkFun (cfName cf) (compileCode $ cfCode cf) | cf <- funs]
               ++ mkReceiveRemote funs
               ++ mkMain
    mkFunDecl name = s"static inline " <> dVoid (cName name) <> parens [dEnv env, dStack stack]
    mkFun name c = mkFunDecl name `scope` (s"PRINT_DEBUG_INFO;" : c)
    header = T.unlines
      $ s"#include \"prelude.h\""
      : [s"#define" `space` rankOf node `space` sshow num
        | (num, (node, _)) <- ncfs']
    mkReceiveRemote funs =
      s"void receive_remote(int code, Stack stack)" `scope`
        (s"switch(code)" `scope`
          ([s"case" `space` sshow (cfName fun) <> s":" `space` 
              cName (cfName fun) <> s"(malloc_env(" <> sshow (fromJust (cfEnvSize fun)) <> s"), stack); break;"
           | fun <- funs, isJust $ cfEnvSize fun]
          ++ [s"default: break;"]))
    mkMain = s"int main(int argc, char** argv)" `scope`
      [ s"initialise(argc, argv);"
      , s"main_loop();"
      ]
    startNodeRank = head [rank | (rank, (node, _)) <- ncfs', node == startNode]
    mkRoot = ("root", T.unlines
      [ s"#define STARTNODE_RANK" `space` sshow startNodeRank
      , s"#define STARTNODE_CODE" `space` sshow startCode
      , root
      ])
