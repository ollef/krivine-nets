module Parser where

import Control.Applicative hiding ((<|>), many)
import Data.Set(Set)
import qualified Data.Set as S
import Text.Parsec
import Text.Parsec.Text

import AST

whitespace :: Parser ()
whitespace = skipMany space

tok :: Parser a -> Parser a
tok p = try p <* whitespace

r :: String -> Parser String
r = tok . string

reserved :: Set String
reserved = S.fromList ["let", "in", "if", "then", "else"]

startIdentLetter :: Parser Char
startIdentLetter = letter

identLetter :: Parser Char
identLetter = startIdentLetter <|> digit

number :: Parser Int
number = read <$> tok (many1 digit)

ident :: Parser String
ident = try (do
  s <- tok ((:) <$> startIdentLetter <*> many identLetter)
  if s `S.member` reserved
  then unexpected $ "reserved identifier: " ++ show s
  else return s) <?> "identifier"

afterwards :: Parser a -> Parser (a -> a) -> Parser a
afterwards p after = do
  result <- p
  f      <- option id after
  return $ f result

term :: Parser Term
term = (abss <$ r"\\" <*> many ident <* r"." <*> term
    <|> lett <$ r"let" <*> ident <*> many ident <* r"=" <*> term <* r"in" <*> term
    <|> If  <$ r"if" <*> term <* r"then" <*> term <* r"else" <*> term
    <|> apps <$> atomicTerm <*> many atomicTerm
    ) `afterwards`
    (primOp <$> op <*> term)
    <?> "term"
  where
    primOp p y x = PrimOp x p y
    op =  Add <$ r"+"
      <|> Mul <$ r"*"
      <|> Sub <$ r"-"
      <|> Div <$ r"/"
      <|> Eq  <$ r"=="
      <|> NEq <$ r"!="
      <|> GEq <$ r">="
      <|> LEq <$ r"<="
      <|> Gr  <$ r">"
      <|> Le  <$ r"<"
      <?> "binary operation"
    apps = foldl App
    abss = flip $ foldr Abs
    lett f xs = Let f . abss xs

atomicTerm :: Parser Term
atomicTerm =  (Var <$> ident
          <|> Lit . LitInt <$> number
          <|> r"(" *> term <* r")")
          `afterwards` (flip AtNode <$ r"@" <*> ident)
          <?> "atomic term"
