{-# LANGUAGE DeriveDataTypeable, RecordWildCards #-}
module Main where

import Control.Monad
import Data.Monoid
import Data.Text(Text, pack)
import qualified Data.Text as T
import qualified Data.Text.IO as TextIO
import System.Console.CmdArgs.Implicit hiding (name)
import System.Directory
import System.FilePath
import Text.Parsec

import Paths_dkrivinec

import qualified AST
import qualified Backend
import qualified ByteCode
import qualified Parser

data Args = Args
  { inputFile :: FilePath
  , outputDir :: FilePath
  , flags     :: String
  } deriving (Show, Data, Typeable)

defArgs :: Mode (CmdArgs Args)
defArgs = cmdArgsMode $ Args
  { inputFile = def   &= argPos 0 &= typFile
  , outputDir = "out" &=             typDir      &= help "Directory to generate output in"
  , flags     = def   &=             typ "FLAGS" &= help "Arguments passed to mpicc"
  } &= summary "DSECD compiler"

createMakefile :: String -> [String] -> Text -> Text
createMakefile flags files template = mconcat
  [ pack "FLAGS=" <> pack flags <> pack "\n"
  , pack "SRC_FILES=" <> T.unwords (map pack files) <> pack "\n"
  , template
  ]

runCompiler :: Args -> IO ()
runCompiler Args{..} = do
  input <- TextIO.readFile inputFile
  case parse Parser.term inputFile input of
    Left err   -> error $ show err
    Right term -> do
      let readDataFile f = TextIO.readFile =<< getDataFileName f
      root     <- readDataFile "root_template.c"
      makefile <- readDataFile "Makefile_template"
      preludePath <- getDataFileName "prelude.h"

      let nodes = Backend.compile "defaultNode" (AST.envSize term) root (ByteCode.compile term)

      createDirectoryIfMissing True outputDir

      copyFile preludePath (outputDir </> "prelude.h")
      nodeNames <- forM nodes $ \(name, code) -> do
        TextIO.writeFile (outputDir </> name <.> "c") code
        return name
      TextIO.writeFile (outputDir </> "Makefile") $
        createMakefile flags nodeNames makefile

main :: IO ()
main = runCompiler =<< cmdArgsRun defArgs
