module ByteCode where

import Data.List
import Data.Maybe
import qualified Data.Set as S

import AST
import Auxiliary

type CVar = Int

data InstrF c
  = PopArg
  | PushArg c
  deriving (Eq, Show)

instance Eq2   InstrF where eq2 = (==)
instance Show2 InstrF where show2 = show

data CodeF c
  = InstrF c :. CodeF c
  | CLit Lit
  | Access CVar
  | Prim PrimOp c c
  | Remote Node Int c
  | Cond c c c
  deriving (Eq, Show)

instance Eq2   CodeF where eq2 = (==)
instance Show2 CodeF where show2 = show

type Code = Fix CodeF

compile :: Term -> Code
compile = In . compile' []
  where
    icompile' vs = In . compile' vs
    compile' :: [Var] -> Term -> CodeF Code
    compile' vs term = case term of
      Var v    -> Access (el vs v)
      Abs v t  -> PopArg :. compile' (v:vs) t
      App t t' -> PushArg (icompile' vs t') :. compile' vs t
      AtNode t n | S.null fvs -> Remote n (envSize t) (icompile' [] t)
                 | otherwise -> compile' vs (foldl App (foldr Abs t lfvs `AtNode` n)
                                                       (map Var lfvs))
        where
          fvs = fv t
          lfvs = S.toList fvs
      Let v t t' -> compile' vs (Abs v t' `App` t)
      Lit l -> CLit l
      PrimOp t p t' -> Prim p (icompile' vs t) (icompile' vs t')
      If t1 t2 t3 -> Cond (icompile' vs t1) (icompile' vs t2) (icompile' vs t3)

    el :: (Eq a, Show a) => [a] -> a -> Int
    el xs x = length xs - 1 - fromMaybe (error $ "Not in scope: " ++ show x) (elemIndex x xs)
