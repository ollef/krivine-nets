#include "prelude.h"

int main(int argc, char** argv) {
  initialise(argc, argv);

  RemotePtr* ptr = remote_ptr(0);
  Stack stack = malloc_stack(ptr, 0);
  free(ptr);

  send_remote(STARTNODE_RANK, STARTNODE_CODE, stack);

  MPI_Status status;
  MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
  int count;
  MPI_Get_count(&status, MPI_INT, &count);

  ReturnMessage msg;
  MPI_Recv(&msg, count, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG,
           MPI_COMM_WORLD, MPI_STATUS_IGNORE);

  printf("******* Result: %" PRIdPTR " ********\n", msg.value);
}
