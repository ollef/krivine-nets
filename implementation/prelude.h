#ifndef __PRELUDE_H__
#define __PRELUDE_H__

#include "mpi.h"
#define __STDC_FORMAT_MACROS
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define MALLOC malloc
#define ERROR(...) fprintf(stderr, "********" __VA_ARGS__); exit(0);
#ifdef NDEBUG
#define PRINT_DEBUG(...) printf(__VA_ARGS__);
#define PRINT_DEBUG_INFO {int rank; MPI_Comm_rank(MPI_COMM_WORLD, &rank); PRINT_DEBUG("%d entering %s\n", rank, __FUNCTION__);}
#else
#define PRINT_DEBUG(...)
#define PRINT_DEBUG_INFO
#endif

/* An int of the architecture's pointer size */
typedef intptr_t int_t;

struct Env_t; typedef struct Env_t* Env;
struct Stack_t; typedef struct Stack_t* Stack;

static const size_t STACK_SIZE = 10000;

typedef void(*Code)(Env, Stack);
/*****************************************************************************
 * Init
 *****************************************************************************/
static inline void initialise(int argc, char** argv) {
  int provided;
  if(MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided) != MPI_SUCCESS) {
    ERROR("MPI_Init failed!\n");
  }
  if(provided != MPI_THREAD_MULTIPLE) {
    printf("******* The MPI environment could not provide the required level of\n"
           "******* thread support. This may lead to deadlocks.\n"
           "******* Proceeding...\n");
  }
}

/* ----------------------------------------------------------------------------
 * Closures
 * ---------------------------------------------------------------------------*/

typedef struct Closure {
  Code code;
  Env env;
} Closure;


/* ----------------------------------------------------------------------------
 * Remote pointers
 * ---------------------------------------------------------------------------*/
typedef struct RemotePtr {
  void* ptr;
  int location;
} RemotePtr;

static inline RemotePtr* remote_ptr(void* val) {
  PRINT_DEBUG_INFO;
  RemotePtr* res = malloc(sizeof(RemotePtr));
  MPI_Comm_rank(MPI_COMM_WORLD, &res->location);
  res->ptr = val;
  return res;
}
/* ----------------------------------------------------------------------------
 * Environment elements
 * ---------------------------------------------------------------------------*/
typedef enum EnvElemTag {
  LOCAL,
  REMOTE
} EnvElemTag;

typedef struct RemoteEnvElem {
  RemotePtr ptr;
  int_t index;
} RemoteEnvElem;

typedef struct EnvElem {
  EnvElemTag tag;
  union {
    Closure local;
    RemoteEnvElem remote;
  } payload;
} EnvElem;

static inline EnvElem local_envelem(Closure closure) {
  return (EnvElem) {.tag = LOCAL, .payload = {.local = closure}};
}

static inline EnvElem remote_envelem(RemotePtr ptr, int_t index) {
  return (EnvElem) {.tag = REMOTE, .payload = {.remote = {.ptr = ptr,
                                                          .index = index}}};
}

void print_envelem(EnvElem el) {
#ifdef NDEBUG
  if(el.tag == LOCAL) {
    PRINT_DEBUG("local");
  } else {
    PRINT_DEBUG("remote@%d[%" PRIdPTR "]", el.payload.remote.ptr.location, el.payload.remote.index);
  }
#endif
}

/* ----------------------------------------------------------------------------
 * Environments
 * ---------------------------------------------------------------------------*/
struct Env_t {
  size_t max_size;
  EnvElem* top;
  EnvElem* bottom;
};

static inline void print_env(Env env) {
#ifdef NDEBUG
  PRINT_DEBUG("Env: ");
  for(EnvElem* v = env->bottom - 1; v >= env->top; --v) {
    print_envelem(*v); PRINT_DEBUG(", ");
  }
  PRINT_DEBUG("[]\n");
#endif
}

static inline size_t size_env(Env env) {
  PRINT_DEBUG_INFO;
  print_env(env);
  return env->bottom - env->top;
}

static inline EnvElem* access_env(Env env, size_t index) {
  PRINT_DEBUG_INFO;
  print_env(env);
  return env->top + index;
}

static inline void push_env(Env env, EnvElem val) {
  PRINT_DEBUG_INFO;
  *env->bottom = val;
  ++env->bottom;
  print_env(env);
}

static inline Env malloc_env(size_t max_size) {
  PRINT_DEBUG_INFO;
  EnvElem* arr = MALLOC(sizeof(EnvElem) * max_size);
  Env res = MALLOC(sizeof(struct Env_t));
  res->top = res->bottom = arr;
  res->max_size = max_size;
  return res;
}

static inline Env copy_env(Env env) {
  PRINT_DEBUG_INFO;
  Env copy = malloc_env(env->max_size);
  for(EnvElem* top = env->top; top < env->bottom; ++top) {
    push_env(copy, *top);
  }
  print_env(env);
  return copy;
}

void enter_closure(Closure closure, Stack stack) {
  closure.code(closure.env, stack);
}

void enter_closure_copy(Closure closure, Stack stack) {
  closure.code(copy_env(closure.env), stack);
}

/* ----------------------------------------------------------------------------
 * Stack elements
 * ---------------------------------------------------------------------------*/
typedef enum StackElemTag {
  ARG,
  IF,
  OP2,
  OP1
} StackElemTag;

typedef struct IfStackElem {
  Closure truecase;
  Closure falsecase;
} IfStackElem;

typedef struct Op2StackElem {
  int_t (*op)(int_t, int_t);
  Closure arg;
} Op2StackElem;

typedef struct Op1StackElem {
  int_t (*op)(int_t, int_t);
  int_t arg;
} Op1StackElem;

typedef struct {
  StackElemTag tag;
  union {
    Closure arg;
    IfStackElem iff;
    Op2StackElem op2;
    Op1StackElem op1;
  } payload;
} StackElem;

StackElem arg_stackelem(Closure arg) {
  return (StackElem) {.tag = ARG, .payload = {.arg = arg}};
}

void print_stackelem(StackElem el) {
#ifdef NDEBUG
  switch(el.tag) {
    case ARG:
      PRINT_DEBUG("arg");
      break;
    case IF:
      PRINT_DEBUG("if");
      break;
    case OP2:
      PRINT_DEBUG("op2");
      break;
    case OP1:
      PRINT_DEBUG("op1");
      break;
  }
#endif
}

/* ----------------------------------------------------------------------------
 * Stacks
 * ---------------------------------------------------------------------------*/
struct Stack_t {
  StackElem* top;
  StackElem* bottom; // One step further than last element
  RemotePtr contptr;
  int_t args;
  int_t drop;
};

static inline void print_stack(Stack stack) {
#ifdef NDEBUG
  PRINT_DEBUG("Stack: ");
  for(StackElem* v = stack->bottom - 1; v >= stack->top; --v) {
    print_stackelem(*v); PRINT_DEBUG(", ");
  }
  PRINT_DEBUG("[], ");
  if(stack->contptr.ptr) {
    PRINT_DEBUG("cont@%d,args=%" PRIdPTR ",drop=%" PRIdPTR "\n",
                stack->contptr.location, stack->args,
                stack->drop);
  }
  else {
    PRINT_DEBUG("end\n");
  }
#endif
}

static inline Stack malloc_stack(RemotePtr* contptr, int_t args) {
  PRINT_DEBUG_INFO;
  StackElem* arr = MALLOC(sizeof(StackElem) * STACK_SIZE);
  Stack res = MALLOC(sizeof(struct Stack_t));
  res->top = res->bottom = arr;
  res->contptr = *contptr;
  res->args = args;
  res->drop = 0;
  return res;
}

static inline EnvElem pop_arg(Stack stack) {
  PRINT_DEBUG_INFO;
  if(stack->bottom <= stack->top) {
    if(stack->args <= 0) {
      goto error;
    }
    EnvElem res = remote_envelem(stack->contptr, stack->drop);
    --stack->args;
    ++stack->drop;
    return res;
  }
  --stack->bottom;
  print_stack(stack);
  StackElem* el = stack->bottom;
  if(el->tag != ARG) {
    goto error;
  }
  return local_envelem(el->payload.arg);
  error:
      ERROR("Trying to pop a stack without args");
}

static inline int_t num_args(Stack stack) {
  PRINT_DEBUG_INFO;
  int_t res = 0;
  for(StackElem* el = stack->bottom - 1; el >= stack->top; --el) {
    if(el->tag == ARG) {
        ++res;
    } else {
      return res;
    }
  }
  res += stack->args;
  print_stack(stack);
  return res;
}

static inline EnvElem access_stack(Stack stack, int_t index) {
  PRINT_DEBUG_INFO;
  print_stack(stack);
  int_t local_stack_size = stack->bottom - stack->top;
  PRINT_DEBUG("local stack size: %" PRIdPTR " index: %" PRIdPTR "\n", local_stack_size, index);
  if(index < local_stack_size) {
    StackElem* el = stack->bottom - index - 1;
    if(el->tag == ARG) {
      return local_envelem(el->payload.arg);
    } else {
      ERROR("Trying to access a non-argument stack element");
    }
  } else {
    index -= local_stack_size;
    if(stack->contptr.ptr && stack->args > index) {
      return remote_envelem(stack->contptr, index + stack->drop);
    } else {
      ERROR("Trying to access a remote non-argument stack element");
    }
  }
}

static inline void drop_stack(Stack stack, int_t drop) {
  PRINT_DEBUG_INFO;
  int_t local_stack_size = stack->bottom - stack->top;
  int_t drop_local = drop;
  int_t drop_remote = 0;
  if(drop > local_stack_size) {
    drop_remote = drop - local_stack_size;
    drop_local = local_stack_size;
  }
  stack->bottom -= drop_local;
  stack->args -= drop_remote;
  stack->drop += drop_remote;
  print_stack(stack);
}

static inline StackElem* pop_stack(Stack stack) {
  PRINT_DEBUG_INFO;
  if(stack->bottom <= stack->top) {
    return 0;
  }
  --stack->bottom;
  print_stack(stack);
  return stack->bottom;
}

static inline void push_stack(Stack stack, StackElem val) {
  PRINT_DEBUG_INFO;
  *stack->bottom = val;
  ++stack->bottom;
  print_stack(stack);
}

static inline Stack* suspend_stack(Stack stack) {
  PRINT_DEBUG_INFO;
  Stack* res = MALLOC(sizeof(Stack));
  *res = stack;
  return res;
}
/* ----------------------------------------------------------------------------
 * Instructions
 * ---------------------------------------------------------------------------*/
static inline void instr_pop_arg(Env env, Stack stack) {
  PRINT_DEBUG_INFO;
  push_env(env, pop_arg(stack));
}

static inline void instr_push_arg(Code code, Env env, Stack stack) {
  PRINT_DEBUG_INFO;
  StackElem sel = arg_stackelem((Closure) {.code = code, .env = copy_env(env)});
  push_stack(stack, sel);
}

static inline void send_return(RemotePtr* p, int_t v, int_t drop);

static inline void instr_lit(Stack stack, int_t n) {
  PRINT_DEBUG_INFO;
  PRINT_DEBUG("lit: %" PRIdPTR "\n", n);
  StackElem* elptr = pop_stack(stack);
  if(elptr == 0) {
    RemotePtr* rptr = malloc(sizeof(RemotePtr));
    *rptr = stack->contptr;
    send_return(rptr, n, stack->drop);
    return;
  }
  StackElem el = *elptr;
  switch(el.tag) {
    case IF: {
      IfStackElem iff = el.payload.iff;
      if(n) {
        enter_closure(iff.truecase,  stack);
        return;
      } else {
        enter_closure(iff.falsecase, stack);
        return;
      }
    }
    case OP2: {
      Op2StackElem op2 = el.payload.op2;
      Op1StackElem op1 = {.op = op2.op, .arg = n};
      push_stack(stack, (StackElem) {.tag = OP1, .payload = {.op1 = op1}});
      enter_closure(op2.arg, stack);
      return;
    }
    case OP1: {
      Op1StackElem op1 = el.payload.op1;
      instr_lit(stack, op1.op(op1.arg, n));
      return;
    }
    default:
      ERROR("LITERAL WITHOUT CONTINUATION");
  }
}

static inline void send_var(RemotePtr* dest, int_t offset, Stack cont);

static inline void instr_access(Env env, Stack stack, int_t index) {
  PRINT_DEBUG_INFO;
  PRINT_DEBUG("env index: %" PRIdPTR "\n", index);
  EnvElem* el = access_env(env, index);
  if(el->tag == LOCAL) {
    enter_closure_copy(el->payload.local, stack);
  } else {
    RemotePtr* rptr = malloc(sizeof(RemotePtr));
    *rptr = el->payload.remote.ptr;
    send_var(rptr, el->payload.remote.index, stack);
  }
}

static inline void instr_primop(Env env, Stack stack, int_t (*op)(int_t, int_t), Code c1, Code c2) {
  PRINT_DEBUG_INFO;
  push_stack(stack, (StackElem) {.tag = OP2, 
                                 .payload = {.op2 = {.op = op,
                                                     .arg = {.code = c2,
                                                             .env = copy_env(env)}}}});
  c1(env, stack);
}

static inline void instr_cond(Env env, Stack stack, Code c1, Code c2, Code c3) {
  PRINT_DEBUG_INFO;
  Env envcopy = copy_env(env);
  push_stack(stack, (StackElem) {.tag = IF,
                                 .payload = {.iff = {.truecase  = {.code = c2,
                                                                   .env = envcopy},
                                                     .falsecase = {.code = c3,
                                                                   .env = envcopy}}}});
  c1(env, stack);
}

static inline void send_remote(int rank, int code, Stack stack);
static inline void instr_remote(Stack stack, int rank, int code) {
  PRINT_DEBUG_INFO;
  send_remote(rank, code, stack);
}

/* ----------------------------------------------------------------------------
 * Operations
 * ---------------------------------------------------------------------------*/
static inline int_t op_add(int_t x, int_t y) {
  return x + y;
}

static inline int_t op_mul(int_t x, int_t y) {
  return x * y;
}

static inline int_t op_sub(int_t x, int_t y) {
  return x - y;
}

static inline int_t op_div(int_t x, int_t y) {
  return x / y;
}

static inline int_t op_eq(int_t x, int_t y) {
  return x == y;
}

static inline int_t op_neq(int_t x, int_t y) {
  return x != y;
}

static inline int_t op_gr(int_t x, int_t y) {
  return x > y;
}

static inline int_t op_le(int_t x, int_t y) {
  return x < y;
}

static inline int_t op_geq(int_t x, int_t y) {
  return x >= y;
}

static inline int_t op_leq(int_t x, int_t y) {
  return x <= y;
}

/* ----------------------------------------------------------------------------
 * Communication
 * ---------------------------------------------------------------------------*/
typedef enum MsgTag {
  REMOTE_MSG_TAG,
  VAR_MSG_TAG,
  RETURN_MSG_TAG
} MsgTag;

typedef struct RemoteMessage {
  int code;
  RemotePtr contptr;
  int_t args;
} RemoteMessage;


typedef struct VarMessage {
  Stack* stackptr;
  int_t offset;
  RemotePtr contptr;
  int_t args;
} VarMessage;

typedef struct ReturnMessage {
  Stack* stackptr;
  int_t value;
  int_t drop;
} ReturnMessage;

static inline void send_remote(int rank, int code, Stack stack) {
  PRINT_DEBUG_INFO;
  RemoteMessage* msg = malloc(sizeof(RemoteMessage));
  RemotePtr* contptr = remote_ptr(suspend_stack(stack));

  *msg = (RemoteMessage) {.code = code,
                          .contptr = *contptr,
                          .args = num_args(stack)};
  free(contptr);
  PRINT_DEBUG("Sending remote message to %d\n", rank);
  MPI_Send(msg, sizeof(RemoteMessage),
           MPI_BYTE, rank, REMOTE_MSG_TAG, MPI_COMM_WORLD);
  free(msg);
}

static inline void send_var(RemotePtr* dest, int_t offset, Stack cont) {
  PRINT_DEBUG_INFO;
  VarMessage* msg = malloc(sizeof(VarMessage));
  RemotePtr* contptr = remote_ptr(suspend_stack(cont));
  *msg = (VarMessage) {.stackptr = (Stack*) dest->ptr,
                       .offset = offset,
                       .args = num_args(cont),
                       .contptr = *contptr};
  free(contptr);
  PRINT_DEBUG("Sending var message to %d\n", dest->location);
  MPI_Send(msg, sizeof(VarMessage),
           MPI_BYTE, dest->location, VAR_MSG_TAG, MPI_COMM_WORLD);
  free(dest);
  free(msg);
}

static inline void receive_var(VarMessage* msg) {
  PRINT_DEBUG_INFO;
  Stack stack = *msg->stackptr;
  Env env = malloc_env(1);
  push_env(env, access_stack(stack, msg->offset));
  Stack newstack = malloc_stack(&msg->contptr, msg->args);
  free(msg);
  instr_access(env, newstack, 0);
}

static inline void send_return(RemotePtr* p, int_t v, int_t drop) {
  PRINT_DEBUG_INFO;
  // We dynamically allocate to help the C compiler to do tail call elimination
  ReturnMessage* msg = malloc(sizeof(ReturnMessage));
  *msg = (ReturnMessage) {.stackptr = (Stack*) p->ptr,
                          .value    = v,
                          .drop     = drop};
  PRINT_DEBUG("Sending return message to %d\n", p->location);
  MPI_Send(msg, sizeof(ReturnMessage),
           MPI_BYTE, p->location, RETURN_MSG_TAG, MPI_COMM_WORLD);
  free(p);
  free(msg);
}

static inline void receive_return(ReturnMessage* msg) {
  PRINT_DEBUG_INFO;
  Stack stack = *msg->stackptr;
  drop_stack(stack, msg->drop);
  int_t val = msg->value;
  free(msg);
  instr_lit(stack, val);
}

void receive_remote(int, Stack);

static inline void main_loop() {
  while(1) {
    PRINT_DEBUG_INFO;
    MPI_Status status;
    MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
    int count;
    MPI_Get_count(&status, MPI_BYTE, &count);
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    PRINT_DEBUG("Node %d received tag: %d\n", rank, status.MPI_TAG);

    switch (status.MPI_TAG) {
      case REMOTE_MSG_TAG: {
        PRINT_DEBUG("received remote msg\n");
        RemoteMessage* msg = malloc(sizeof(RemoteMessage));
        MPI_Recv(msg, count, MPI_BYTE, MPI_ANY_SOURCE, MPI_ANY_TAG,
                 MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        Stack stack = malloc_stack(&msg->contptr, msg->args);
        int c = msg->code;
        free(msg);
        receive_remote(c, stack);
        break;
      }
      case RETURN_MSG_TAG: {
        PRINT_DEBUG("received return msg\n");
        ReturnMessage* msg = malloc(sizeof(ReturnMessage));
        MPI_Recv(msg, count, MPI_BYTE, MPI_ANY_SOURCE, MPI_ANY_TAG,
                 MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        receive_return(msg);
        break;
      }
      case VAR_MSG_TAG: {
        PRINT_DEBUG("received var msg\n");
        VarMessage* msg = malloc(sizeof(VarMessage));
        MPI_Recv(msg, count, MPI_BYTE, MPI_ANY_SOURCE, MPI_ANY_TAG,
                 MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        receive_var(msg);
        break;
      }
      default: break;
    }
  }
}

#endif
