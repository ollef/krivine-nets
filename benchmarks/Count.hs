module Count where

count :: FilePath -> IO ()
count file = do
  f <- readFile file
  let xs  = map read $ lines f :: [Int]
      avg = fromIntegral (sum xs) / fromIntegral (length xs) :: Double
      m   = maximum xs
  putStrLn $ "num: " ++ show (length xs)
  putStrLn $ "avg: " ++ show avg
  putStrLn $ "max: " ++ show m
